import React, { useState } from 'react'
import 'bootstrap'
import Container from 'react-bootstrap/Container';
import Navbar from 'react-bootstrap/Navbar';
import Logo from '../assets/toggl-logo.png';
import './Header.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faLeftLong } from '@fortawesome/free-solid-svg-icons';
import Sidebar from './Sidebar';
import { Button } from 'react-bootstrap';


const Header = ({onSubmit}) => {
const[show,setShow] = useState(false)

  const handleClose = () => setShow(false)
  const handleShow = () => setShow(true)

  return (
    <Navbar expand="lg" sticky='top'
        className='bg-body-tertiary'
        bg="dark"
        data-bs-theme="dark">
        
        <Sidebar show={show} handleClose={handleClose} onSubmit={onSubmit}/>
        <Container>
            <Navbar.Brand href="#home">
                <img src={Logo} alt="Toggl logo" className='brand-logo'/>
            </Navbar.Brand>
            <Button variant="primary" onClick={handleShow}>
                <FontAwesomeIcon icon={faLeftLong}/>
            </Button>
        </Container>
    </Navbar>
  )
}

export default Header
