import React, { useEffect, useState } from 'react';
import Header from './components/Header';
import Sidebar from './components/Sidebar';
import ProjectCard from './components/ProjectCard';
import { Container, Row } from 'react-bootstrap';
import { v4 as uuid } from 'uuid';
import EditProjectModal from './components/EditProjectModal';

export default function App() {
  // const description = "Lorem ipsum dolor sit amet";
  // const arr = [
  //   {id: "1",name: "Project 1", price: "45", description, status: "Ongoing", elapsed:0, runningSince:null},
  //   {id: "2",name: "Project 2", price: "65", description, status: "Ongoing", elapsed:0, runningSince:null},
  //   {id: "3",name: "Project 2", price: "65", description, status: "Completed", elapsed:0, runningSince:null}
  // ];
  const [projects, setProjects] = useState(JSON.parse(localStorage.getItem('projects')) || []);

  const[modalShow, setModalShow] = useState(false);
  const[editProject, setEditProject] = useState(false);

  useEffect(() => {
    localStorage.setItem("projects", JSON.stringify(projects));
  }, [projects]);

  function handleModalClose() {
    setModalShow(false);
    setEditProject(null);
  }

  function handleStartTimer(projectId){
    setProjects(
      projects.map(project => {
        if (project.id === projectId) {
          return{...project, runningSince: Date.now()}
        }
        return project;
      })
    );
  }

  function handleStopTimer(projectId) {
    setProjects(
      projects.map((project)=> {
        if (project.id===projectId) {
          const totalElapsed  = project.elapsed + (Date.now()-project.runningSince);
          return {... project, runningSince: null, elapsed: totalElapsed}
      }
      return project;
    })
   )
  }
  
  function handleAddProject(project){
    const newProject = {
      id : uuid(),
      name : project.name,
      description : project.description,
      price : project.price,
      status : project.status,
      elapsed: 0,
      runningSince: null
    }
    setProjects([...projects,newProject]);
  }

  function handleDelete(projectId) {
    setProjects(projects.filter(project=> project.id !== projectId))
  }

  function handleUpdateProject(updatedProject, projectId){
    setProjects(projects.map (project=>{
      if(project.id===projectId){
        return{
          ...project,
          name: updatedProject.name,
          description: updatedProject.description,
          price: updatedProject.price,
          status: updatedProject.status
        }
      }
      return project;
    }));
  }

  function handleEdit(projectId) {
    setModalShow(true);

    for(let i = 0; i<projects.length; i++) {
      if(projects[i].id === projectId) {
        setEditProject(projects[i]);
        break;
      }
    }
  }

  return (
    <>
      <Header onSubmit={handleAddProject}/>
      <Container className="mt-3">
        <Row>
          { projects.map(project =>(
            <ProjectCard
            key={project.id}
            project={project}
            onTimerStart = {handleStartTimer}
            onTimerStop = {handleStopTimer}
            onEdit = {handleEdit}
            onDelete={handleDelete}
            />)
           )
          }
        </Row>
        <EditProjectModal
           show = {modalShow}
           onClose = {handleModalClose}
           project={editProject}
           onSubmit={handleUpdateProject}
        />
        </Container>
      <Sidebar/>
    </>
  );
}



